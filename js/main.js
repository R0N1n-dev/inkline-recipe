new Vue({
    el: "#app",
    data() {
        return {
            query: "",
            meals: undefined,
        };
    },
    methods: {
        getIt: function(e) {
            if (e.key == "Enter") {
                fetch(
                        `https://www.themealdb.com/api/json/v1/1/search.php?s=${this.query}`
                    )
                    .then((res) => res.json())
                    .then(this.setResults);
            }
        },
        setResults: function(data) {
            this.meals = data;
            this.query = "";
        },
    },
});

